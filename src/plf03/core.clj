(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [x] (range x))
        h (fn [x xs] (+ x (apply + xs)))
        z (comp f g h)]
    (z 2 [1 2 3 4 5])))

(defn función-comp-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (- x 1000))
        h (fn [xs] (apply + xs))
        i (fn [xs] (filter odd? xs))
        z (comp f g h i)]
    (z [1 2 3 4 5 6 7 8])))

(defn función-comp-3
  []
  (let [f (fn [x] (str x))
        g (fn [x] (inc x))
        h (fn [x y z] (+ x y z))
        z (comp f g h)]
    (z 1 2 3)))

(defn función-comp-4
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (rest xs))
        h (fn [xs] (rest xs))
        z (comp f g h)]
    (z '(:naranja :manzana :pera :platano))))


(defn función-comp-5
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [x] (range x))
        h (fn [a b] (+ a b))
        z (comp f g h)]
    (z 8 9)))

(defn función-comp-6
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (rest xs))
        h (fn [xs] (reverse xs))
        z (comp f g h)]
    (z [1 2 3 4 5 6 7 8])))

(defn función-comp-7
  []
  (let [f (fn [x] (if (= x true) "Es una vocal" "No es una vocal"))
        g (fn [x] (contains? #{"a" "e" "i" "o" "u"} x))
        z (comp f g)]
    (z "c")))

(defn función-comp-8
  []
  (let [f (fn [x] (/ x 2))
        g (fn [x] (* x x))
        h (fn [xs x] (* (count xs) x))
        z (comp f g h)]
    (z [1 2 3 4 5] 6)))

(defn función-comp-9
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (filter even? xs))
        h (fn [xs] (map inc xs))
        z (comp f g h)]
    (z [1 2 3 4 5 6 7 8 9])))

(defn función-comp-10
  []
  (let [f (fn [x] (* x x))
        g (fn [xs] (count xs))
        h (fn [x] (filter even? (range x)))
        z (comp f g h)]
    (z 20)))

(defn función-comp-11
  []
  (let [f (fn [xs] (into [] xs))
        g (fn [xs] (filter odd? xs))
        h (fn [xs] (map inc xs))
        i (fn [x] (range x))
        z (comp f g h i)]
    (z 10)))

(defn función-comp-12
  []
  (let [f (fn [xs] (reduce + xs))
        g (fn [xs] (concat xs (range 10)))
        h (fn [xs x] (conj xs x))
        z (comp f g h)]
    (z [1 2 3 4 5] 6)))

(defn función-comp-13
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (filter nil? xs))
        z (comp f g)]
    (z [1 nil nil 2 3 4 nil 4 nil 5 nil])))

(defn función-comp-14
  []
  (let [f (fn [x] (* x 2))
        g (fn [x] (- x 10))
        h (fn [x] (+ x x))
        i (fn [x] (* x x))
        z (comp f g h i)]
    (z 10)))

(defn función-comp-15
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [x] (reverse (range x)))
        h (fn [xs] (apply max xs))
        z (comp f g h)]
    (z [8 9 2 3 4 62 2 4 6 7 2 3])))

;;Factorial
(defn función-comp-16
  []
  (let [f (fn [xs] (reduce * xs))
        g (fn [x] (range 2 x))
        h (fn [x] (+ x 1))
        z (comp f g h)]
    (z 5)))

(defn función-comp-17
  []
  (let [f (fn [xs] (keys xs))
        g (fn [xs] (dissoc xs :b))
        h (fn [xs] (assoc xs :c 3))
        z (comp f g h)]
    (z {:a 1 :b 2 :c 3 :d 4 :e 5})))

(defn función-comp-18
  []
  (let [f (fn [x] (str "Área del triángulo: " x))
        g (fn [x] (/ x 2))
        h (fn [b h] (* b h))
        z (comp f g h)]
    (z 2 3)))

(defn función-comp-19
  []
  (let [f (fn [x] (+ x 10))
        g (fn [x] (* x x))
        h (fn [xs] (apply / xs))
        z (comp f g h)]
    (z [1 2 3 4 5 6])))

(defn función-comp-20
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [x] (range x))
        h (fn [xs] (peek xs))
        z (comp f g h)]
    (z [1 4 2 4 8 4 2 4])))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z true)))

(defn función-complement-2
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z \c)))

(defn función-complement-3
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z [1 2 3 4 5 6])))

(defn función-complement-4
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 1M)))

(defn función-complement-5
  []
  (let [f (fn [xs] (list xs))
        z (complement f)]
    (z "Hola")))

(defn función-complement-6
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 34)))

(defn función-complement-7
  []
  (let [f (fn [xs] (string? xs))
        z (complement f)]
    (z "Clojure")))

(defn función-complement-8
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 2)))

(defn función-complement-9
  []
  (let [f (fn [pred coll] (not-any? pred coll))
        z (complement f)]
    (z odd? '(2 4 6 8))))

(defn función-complement-10
  []
  (let [f (fn [pred coll] (every? pred coll))
        z (complement f)]
    (z even? '(1 2 3 4 5))))

(defn función-complement-11
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z [1 2 3 4 5 6])))

(defn función-complement-12
  []
  (let [f (fn [xs] (set? xs))
        z (complement f)]
    (z (hash-set 1 2 3 4 5))))

(defn función-complement-13
  []
  (let [f (fn [x] (rational? x))
        z (complement f)]
    (z 1)))

(defn función-complement-14
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 3.5)))

(defn función-complement-15
  []
  (let [f (fn [x] (integer? x))
        z (complement f)]
    (z 10)))

(defn función-complement-16
  []
  (let [f (fn [xs] (sequential? xs))
        z (complement f)]
    (z [1 2 3 4 5])))

(defn función-complement-17
  []
  (let [f (fn [xs] (indexed? xs))
        z (complement f)]
    (z [4 5 6 8 3 5 2])))

(defn función-complement-18
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z :x)))

(defn función-complement-19
  []
  (let [f (fn [x] (map-entry? (first x)))
        z (complement f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-complement-20
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z :clojure)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [f true
        z (constantly f)]
    (z 3 4 3 "A")))

(defn función-constantly-2
  []
  (let [f '(1 2 3 4)
        z (constantly f)]
    (z [1 2 3 4])))

(defn función-constantly-3
  []
  (let [f [\a \b \c \d \e]
        z (constantly f)]
    (z "h" 3 2 :f)))

(defn función-constantly-4
  []
  (let [f "Hola Mundo"
        z (constantly f)]
    (z [:a :b :c :d :f])))

(defn función-constantly-5
  []
  (let [f 10
        z (constantly f)]
    (z 10 23 23 1 2 5 6)))

(defn función-constantly-6
  []
  (let [f #{1 2 3 4 5}
        z (constantly f)]
    (z #{4 5 2 1 3})))

(defn función-constantly-7
  []
  (let [f "A E I O U"
        z (constantly f)]
    (z "A B C D E")))

(defn función-constantly-8
  []
  (let [f 100
        z (constantly f)]
    (z 101)))


(defn función-constantly-9
  []
  (let [f {:a 1 :b 2 :c 3 :d 4}
        z (constantly f)]
    (z {:a 1 :b 2})))

(defn función-constantly-10
  []
  (let [f [10 20 30 40 50 60]
        z (constantly f)]
    (z "Hola" 23 45 23 34)))

(defn función-constantly-11
  []
  (let [f [true true false false true]
        z (constantly f)]
    (z [true true true true])))

(defn función-constantly-12
  []
  (let [f '(:d :c :r :j :t)
        z (constantly f)]
    (z '(1 2 3 4 5))))

(defn función-constantly-13
  []
  (let [f 3500
        z (constantly f)]
    (z 3501)))

(defn función-constantly-14
  []
  (let [f 3.141592
        z (constantly f)]
    (z 3.14)))

(defn función-constantly-15
  []
  (let [f 2.71
        z (constantly f)]
    (z 2.701)))

(defn función-constantly-16
  []
  (let [f [\a \b \c \d \e]
        z (constantly f)]
    (z "abcde")))

(defn función-constantly-17
  []
  (let [f #{1 2 3 4 5}
        z (constantly f)]
    (z [1 2 3 4 5])))

(defn función-constantly-18
  []
  (let [f false
        z (constantly f)]
    (z true)))

(defn función-constantly-19
  []
  (let [f []
        z (constantly f)]
    (z nil)))

(defn función-constantly-20
  []
  (let [f ["a" "e" "i" "o" "u"]
        z (constantly f)]
    (z 23 "a" true 1)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 9 11)))

(defn función-every-pred-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)]
    (z 0 2/3 -2/3 1/4 -1/10)))

(defn función-every-pred-3
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (coll? x))
        z (every-pred f g)]
    (z "Holamundo")))

(defn función-every-pred-4
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (float? x))
        z (every-pred f g)]
    (z 3.0 2.0 5.0 3.4 5.6)))

(defn función-every-pred-5
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (coll? x))
        z (every-pred f g)]
    (z [\a \b \c \d])))

(defn función-every-pred-6
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        z (every-pred f g)]
    (z 2 4 6 8 10)))

(defn función-every-pred-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 9 11 -3)))

(defn función-every-pred-8
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 9 11)))

(defn función-every-pred-9
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 9 11)))

(defn función-every-pred-10
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 3 9 11)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (every? even? x))
        g (fn [x] (vector? x))
        z (every-pred f g)]
    (z [2 4 6 8 10])))

(defn función-every-pred-12
  []
  (let [f (fn [x] (not-any? nil? x))
        g (fn [x] (vector? x))
        z (every-pred f g)]
    (z [true false false true false])))

(defn función-every-pred-13
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos-int? x))
        h (fn [x] (number? x))
        z (every-pred f g h)]
    (z 2 9223372036854775806)))

(defn función-every-pred-14
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (integer? x))
        z (every-pred f g)]
    (z 1)))

(defn función-every-pred-15
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (number? x))
        z (every-pred f g h)]
    (z 2 4 6 8)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (decimal? x))
        g (fn [x] (some? x))
        z (every-pred f g)]
    (z 1M)))

(defn función-every-pred-17
  []
  (let [f (fn [x] (< x 20))
        g (fn [x] (> x 10))
        z (every-pred f g)]
    (z 11 12 13 14 15 16 17 18 19)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (nil? x))
        z (every-pred f g)]
    (z 3 9 11 nil 3 4 6 22 4 5)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (> (count x) 0))
        g (fn [x] (coll? x))
        z (every-pred f g)]
    (z [1 2] [3 4] [5 6])))

(defn función-every-pred-20
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (> (* x 2) 10))
        h (fn [x] (number? x))
        z (every-pred f g h)]
    (z 3 9 11 2 3 4 6)))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [nombre] (str "Hola " nombre))
        z (fnil f "Jorge")]
    (z nil)))

(defn función-fnil-2
  []
  (let [f (fn [a b] (+ a b))
        z (fnil f 2 3)]
    (z nil nil)))

(defn función-fnil-3
  []
  (let [f (fn [xs] (filter even? xs))
        z (fnil f [1 2 3 4 5])]
    (z nil)))

(defn función-fnil-4
  []
  (let [f (fn [x] (inc x))
        z (fnil f 10)]
    (z nil)))

(defn función-fnil-5
  []
  (let [f (fn [x] (take x (repeat (* x 2))))
        z (fnil f 10)]
    (z nil)))

(defn función-fnil-6
  []
  (let [f (fn [a b] [(+ a b) (- a b) (* a b)])
        z (fnil f 5 7)]
    (z nil nil)))

(defn función-fnil-7
  []
  (let [f (fn [x] (take 5 (range x)))
        z (fnil f 10)]
    (z nil)))

(defn función-fnil-8
  []
  (let [f (fn [xs] (reverse xs))
        z (fnil f [1 2 3 4 5 6 7 8 9])]
    (z nil)))

(defn función-fnil-9
  []
  (let [f (fn [xs] (/ (reduce + xs) (count xs)))
        z (fnil f [1 6 8 4 4 5 6 6 3])]
    (z nil)))

(defn función-fnil-10
  []
  (let [f (fn [x] (or (= x 10) (= x 15)))
        z (fnil f 15)]
    (z nil)))

(defn función-fnil-11
  []
  (let [f (fn [xs] (reduce + xs))
        z (fnil f [1 2 3 4 5 6 7 8 9])]
    (z nil)))

(defn función-fnil-12
  []
  (let [f (fn [xs] (coll? xs))
        z (fnil f [:a :b :c :d])]
    (z nil)))

(defn función-fnil-13
  []
  (let [f (fn [a b c] (> (max a b c) 10))
        z (fnil f 1 5 15)]
    (z nil nil nil)))

(defn función-fnil-14
  []
  (let [f (fn [xs] (count (filter odd? xs)))
        z (fnil f [2 3 5 6 7 8 3 2])]
    (z nil)))

(defn función-fnil-15
  []
  (let [f (fn [x] (into [] (filter odd? (map inc (range x)))))
        z (fnil f 10)]
    (z nil)))

(defn función-fnil-16
  []
  (let [f (fn [x] (reduce * (range 2 (+ x 1))))
        z (fnil f 5)]
    (z nil)))

(defn función-fnil-17
  []
  (let [f (fn [xs] (first (rest (rest xs))))
        z (fnil f [1 2 3 4 5 6 7 8 9])]
    (z nil)))

(defn función-fnil-18
  []
  (let [f (fn [a b] (max a (* b b)))
        z (fnil f 2 4)]
    (z nil nil)))

(defn función-fnil-19
  []
  (let [f (fn [a b c] (* (max a b c) (+ a b)))
        z (fnil f 2 5 7)]
    (z nil nil nil)))

(defn función-fnil-20
  []
  (let [f (fn [a b c] (/ (* a a) (* b b) (* c c) 3))
        z (fnil f 2 3 4)]
    (z nil nil nil)))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (count xs))
        z (juxt f g)]
    (z "Clojure")))

(defn función-juxt-2
  []
  (let [f (fn [a b c] (+ a b c))
        g (fn [a b c] (* a b c))
        h (fn [a b c] (min a b c))
        i (fn [a b c] (max a b c))
        z (juxt f g h i)]
    (z 3 4 6)))

(defn función-juxt-3
  []
  (let [f (fn [x xs] (take x xs))
        g (fn [x xs] (drop x xs))
        z (juxt f g)]
    (z 3 [1 2 3 4 5 6])))

(defn función-juxt-4
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (filter odd? xs))
        z (juxt f g)]
    (z [0 1 2 3 4 5 6 7 8 9])))

(defn función-juxt-5
  []
  (let [f (fn [xs] (filter remove xs))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z [-1 -2 4 5 3 -9])))

(defn función-juxt-6
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (into [1 2 3] xs))
        h (fn [xs] (filter odd? xs))
        i (fn [xs] (map inc xs))
        z (juxt f g h i)]
    (z [4 5 6 7 8 9])))

(defn función-juxt-7
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (integer? x))
        h (fn [x] (double? x))
        i (fn [x] (float? x))
        j (fn [x] (char? x))
        k (fn [x] (boolean? x))
        z (juxt f g h i j k)]
    (z true)))

(defn función-juxt-8
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (reduce + xs))
        h (fn [xs] (filter odd? xs))
        i (fn [xs] (map even? xs))
        z (juxt f g h i)]
    (z [0 1 2 3 4 5 6 7 8 9])))

(defn función-juxt-9
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (neg? x))
        z (juxt f g)]
    (z -1)))

(defn función-juxt-10
  []
  (let [f (fn [xs] (reduce * xs))
        g (fn [xs] (reduce - xs))
        h (fn [xs] (reduce + xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6 7 8 9])))


(defn función-juxt-11
  []
  (let [f (fn [xs] (drop-last 3 xs))
        g (fn [xs] (drop-while pos? xs))
        h (fn [xs] (reverse xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6])))

(defn función-juxt-12
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (set? xs))
        h (fn [xs] (vector? xs))
        i (fn [xs] (list? xs))
        z (juxt f g h i)]
    (z #{1 2 3 4 5})))

(defn función-juxt-13
  []
  (let [f (fn [xs] (first xs))
        g (fn [xs] (reverse xs))
        h (fn [xs] (rest xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6 7])))

(defn función-juxt-14
  []
  (let [f (fn [xs] (keys xs))
        g (fn [xs] (dissoc xs :b))
        h (fn [xs] (assoc xs :c 3))
        z (juxt f g h)]
    (z {:a 1 :b 2 :d 3})))

(defn función-juxt-15
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (inc x))
        h (fn [x] (* x x))
        z (juxt f g h)]
    (z 17)))

(defn función-juxt-16
  []
  (let [f (fn [xs] (not-every? even? xs))
        g (fn [xs] (not-every? odd? xs))
        z (juxt f g)]
    (z '(1 2 3 4 5 6 7 8 9 10))))

(defn función-juxt-17
  []
  (let [f (fn [x xs] (drop x xs))
        g (fn [x xs] (take x xs))
        h (fn [x xs] (drop-last x xs))
        z (juxt f g h)]
    (z 2 [0 1 2 3 4 5 6 7 8 9])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (split-with even? xs))
        g (fn [xs] (split-with odd? xs))
        h (fn [xs] (split-with (partial >= 3) xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6 7 8 9])))

(defn función-juxt-19
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (> (* x 2) 10))
        h (fn [x] (number? x))
        z (juxt f g h)]
    (z 10)))

(defn función-juxt-20
  []
  (let [f (fn [xs] (count xs))
        g (fn [xs] (filter nil? xs))
        z (juxt f g)]
    (z [1 2 nil 8 9 10 nil])))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [x y z] (hash-set x y z))
        z (partial f 10 20)]
    (z 30)))

(defn función-partial-2
  []
  (let [f (fn [x y z] (+ x y z))
        z (partial f 56)]
    (z 90 34)))

(defn función-partial-3
  []
  (let [f (fn [a b] (* a b))
        z (partial f 8)]
    (z 8)))

(defn función-partial-4
  []
  (let [f (fn [a b c] (- a b c))
        z (partial f 34 21)]
    (z 1020)))

(defn función-partial-5
  []
  (let [f (fn [x xs] (apply x xs))
        z (partial f +)]
    (z [1 2 3 4 5 6 7 8 9])))

(defn función-partial-6
  []
  (let [f (fn [x xs] (drop x xs))
        z (partial f 2)]
    (z [9 8 7 6 5 4 3 2])))

(defn función-partial-7
  []
  (let [f (fn [a b] (* (+ a b) (- a b)))
        z (partial f 5)]
    (z 10)))

(defn función-partial-8
  []
  (let [f (fn [x1 x2] (str x1 x2))
        z (partial f "Hola ")]
    (z "Mundo")))

(defn función-partial-9
  []
  (let [f (fn [x y z] (max x y z))
        z (partial f 100)]
    (z 200 300)))

(defn función-partial-10
  []
  (let [f (fn [x y z] (min x y z))
        z (partial f 50)]
    (z 10 30)))

(defn función-partial-11
  []
  (let [f (fn [username dominio] (str username "@" dominio))
        z (partial f "jorge")]
    (z "gmail.com")))

(defn función-partial-12
  []
  (let [f (fn [a b c] (/ (* a a) (* b b) (* c c) 3))
        z (partial f 2 3)]
    (z 4)))

(defn función-partial-13
  []
  (let [f (fn [x xs] (+ x (apply + xs)))
        z (partial f 3)]
    (z [1 2 3 4 5 6])))

(defn función-partial-14
  []
  (let [f (fn [xs x] (* (count xs) x))
        z (partial f [1 2 3 4 5 6 7])]
    (z 4)))

(defn función-partial-15
  []
  (let [f (fn [x xs] (concat xs (range x)))
        z (partial f 10)]
    (z [8 7 2 3 5 6 2 3 5 2])))

(defn función-partial-16
  []
  (let [f (fn [a b] (+ (* a b) (+ a b) (- a b)))
        z (partial f 1)]
    (z 8)))

(defn función-partial-17
  []
  (let [f (fn [a b] (if (< a b) b a))
        z (partial f 10)]
    (z 20)))

(defn función-partial-18
  []
  (let [f (fn [xs] (filter odd? xs))
        z (partial f [1 2 3 4 5 6])]
    (z)))

(defn función-partial-19
  []
  (let [f (fn [xs] (map pos? xs))
        z (partial f)]
    (z [9 8 2 3 -2 -3 -5 1])))

(defn función-partial-20
  []
  (let [f (fn [x x2] (take x (range x)))
        z (partial f 5)]
    (z 100)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 7 3 1)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (ratio? x))
        z (some-fn f g)]
    (z 0 4/6 2 4 5/3)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (coll? x))
        z (some-fn f g)]
    (z "Clojure")))

(defn función-some-fn-4
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (float? x))
        z (some-fn f g)]
    (z 5.0 4.0 3.0 2.4 2.4)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (coll? x))
        z (some-fn f g)]
    (z [\a \b \c \d])))

(defn función-some-fn-6
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (even? x))
        z (some-fn f g)]
    (z 2 4 6 8 10)))

(defn función-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 3 9 11 -3)))

(defn función-some-fn-8
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 3 9 11)))

(defn función-some-fn-9
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 3 5 7 2 5 6)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (some-fn f g)]
    (z 0 4 3 4 3)))

(defn función-some-fn-11
  []
  (let [f (fn [x] (every? even? x))
        g (fn [x] (vector? x))
        z (some-fn f g)]
    (z [2 4 6 8 10] [2 3 4 6])))

(defn función-some-fn-12
  []
  (let [f (fn [x] (not-any? nil? x))
        g (fn [x] (vector? x))
        z (some-fn f g)]
    (z [true false false false true])))

(defn función-some-fn-13
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos-int? x))
        h (fn [x] (number? x))
        z (some-fn f g h)]
    (z 2 234232353453 24234)))

(defn función-some-fn-14
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (number? x))
        z (some-fn f g)]
    (z 1 3.5)))

(defn función-some-fn-15
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (pos? x))
        h (fn [x] (number? x))
        z (some-fn f g h)]
    (z 3 3 5 7 9)))

(defn función-some-fn-16
  []
  (let [f (fn [x] (decimal? x))
        g (fn [x] (some? x))
        z (some-fn f g)]
    (z 1M)))

(defn función-some-fn-17
  []
  (let [f (fn [x] (< x 10))
        z (some-fn f)]
    (z 10 11)))

(defn función-some-fn-18
  []
  (let [f (fn [x] (< (* x x) 10))
        g (fn [x] (< (+ x x) 10))
        z (some-fn f g)]
    (z 1 2 3 4)))

(defn función-some-fn-19
  []
  (let [f (fn [x] (< x 20))
        g (fn [x] (> x 10))
        z (some-fn f g)]
    (z 1 2 3 4 5 6 7)))

(defn función-some-fn-20
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (nil? x))
        z (some-fn f g)]
    (z \a)))


(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)